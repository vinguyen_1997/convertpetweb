package com.pnv.models;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "bill_detail"
)
public class BillDetail {

	@Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "bill_detail_id", unique = true, nullable = false)
	private long bill_detail_id;
	
	@ManyToOne(optional=false) 
    @JoinColumn(name="product_id", nullable=false, updatable=false)
	private Product product;
	
	@ManyToOne(optional=false) 
    @JoinColumn(name="bill_id", nullable=false, updatable=false)
	private Bill bill;
	
	@Column(name = "price", unique = true, nullable = false)
	private double price;
	
	@Column(name = "quantity", unique = true, nullable = false)
	private int quantity;

	public BillDetail() {
		super();
	}

	public BillDetail(long bill_detail_id, double price, int quantity) {
		super();
		this.bill_detail_id = bill_detail_id;
		//this.bill_id = bill_id;
		//this.product_id = product_id;
		this.price = price;
		this.quantity = quantity;
	}

	public long getBill_detail_id() {
		return bill_detail_id;
	}

	public void setBill_detail_id(long bill_detail_id) {
		this.bill_detail_id = bill_detail_id;
	}

	
	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public Bill getBill() {
		return bill;
	}

	public void setBill(Bill bill) {
		this.bill = bill;
	}
	
	
	
	
}
