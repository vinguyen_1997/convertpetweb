package com.pnv.models;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.joda.time.DateTime;

@Entity
@Table(name = "bill"
)
public class Bill{

	@Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "bill_id", unique = true, nullable = false)
	private long bill_id;
	
	@Column(name = "total", unique = true, nullable = false)
	private double total;
	
	@Column(name = "payment", unique = true, nullable = false)
	private String payment;
	
	@Column(name = "address", unique = true, nullable = false)
	private String address;
	
	@Column(name = "date", unique = true, nullable = false)
	private DateTime date;
	
	 @OneToMany(fetch = FetchType.LAZY, mappedBy = "bill")
	 private List<BillDetail> billDetails;
	 
	 @ManyToOne(optional=false) 
	 @JoinColumn(name="user_id", nullable=false, updatable=false)
	 private User user;

	public Bill() {
		super();
	}

	public Bill(long bill_id, double total, String payment, String address, DateTime date) {
		super();
		this.bill_id = bill_id;
		//this.user_id = user_id;
		this.total = total;
		this.payment = payment;
		this.address = address;
		this.date = date;
	}

	public long getBill_id() {
		return bill_id;
	}

	public void setBill_id(long bill_id) {
		this.bill_id = bill_id;
	}


	public double getTotal() {
		return total;
	}

	public void setTotal(double total) {
		this.total = total;
	}

	public String getPayment() {
		return payment;
	}

	public void setPayment(String payment) {
		this.payment = payment;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public DateTime getDate() {
		return date;
	}

	public void setDate(DateTime date) {
		this.date = date;
	}

	public List<BillDetail> getBillDetails() {
		return billDetails;
	}

	public void setBillDetails(List<BillDetail> billDetails) {
		this.billDetails = billDetails;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
	
	
}
