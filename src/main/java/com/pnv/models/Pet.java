package com.pnv.models;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "pet"
)
public class Pet {
	
	
	@Id
	@Column(name = "id_pro")
	private long id_pro;
	
	@Column(name = "weight", unique = true, nullable = false)
	private String weight;
	
	@Column(name = "color", unique = true, nullable = false)
	private String color;
	
	@Column(name = "resource", unique = true, nullable = false)
	private String resource;
	
	@Column(name = "sell_pet", unique = true, nullable = false)
	private long sell_pet;
	
	
	@OneToOne(optional=false) 
    @JoinColumn(name="id_pro", nullable=false, updatable=false)
	@JsonIgnore
	private Product product;
	
	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public Pet() {
		super();
	}

	public Pet(long id_pro, String weight, String color, String resource, long sell_pet) {
		super();
		this.id_pro = id_pro;
		this.weight = weight;
		this.color = color;
		this.resource = resource;
		this.sell_pet = sell_pet;
		
	}

	public long getId_pro() {
		return id_pro;
	}

	public void setId_pro(long id_pro) {
		this.id_pro = id_pro;
	}

	public String getWeight() {
		return weight;
	}

	public void setWeight(String weight) {
		this.weight = weight;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getResource() {
		return resource;
	}

	public void setResource(String resource) {
		this.resource = resource;
	}

	public long getSell_pet() {
		return sell_pet;
	}

	public void setSell_pet(long sell_pet) {
		this.sell_pet = sell_pet;
	}

	
	
	
}
