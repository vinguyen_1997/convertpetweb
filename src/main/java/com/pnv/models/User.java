package com.pnv.models;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "users"
)
public class User {

	@Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "user_id", unique = true, nullable = false)
	private long user_id;
	
	@Column(name = "user_email", unique = true, nullable = false)
	private String user_email;
	
	@Column(name = "user_pass", unique = true, nullable = false)
	private String user_pass;
	
	@Column(name = "user_role", unique = true, nullable = false)
	private boolean user_role;
	
	 @OneToMany(fetch = FetchType.LAZY, mappedBy = "user")
	 private List<Bill> bills;

	public User() {
		super();
	}

	public User(long user_id, String user_email, String user_pass, boolean user_role) {
		super();
		this.user_id = user_id;
		this.user_email = user_email;
		this.user_pass = user_pass;
		this.user_role = user_role;
	}

	public long getUser_id() {
		return user_id;
	}

	public User(String user_email, String user_pass) {
		super();
		this.user_email = user_email;
		this.user_pass = user_pass;
	}

	public void setUser_id(long user_id) {
		this.user_id = user_id;
	}

	public String getUser_email() {
		return user_email;
	}

	public void setUser_email(String user_email) {
		this.user_email = user_email;
	}

	public String getUser_pass() {
		return user_pass;
	}

	public void setUser_pass(String user_pass) {
		this.user_pass = user_pass;
	}

	public boolean getUser_role() {
		return user_role;
	}

	public void setUser_role(boolean user_role) {
		this.user_role = user_role;
	}
	
	
}
