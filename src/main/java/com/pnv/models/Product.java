package com.pnv.models;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "product"
)
public class Product {

	@Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "product_id", unique = true, nullable = false)
    private long product_id;
	
	@Column(name = "category_id", unique = true, nullable = false)
	private long category_id;
	 
	@Column(name = "product_name", unique = true, nullable = false)
	private String product_name;
	
	 @Column(name = "product_image", unique = true, nullable = false)
	private String product_image;
	 
	 @Column(name = "product_price", unique = true, nullable = false)
	private double product_price;
	 
	 @Column(name = "product_description", unique = true, nullable = false)
	private String product_description;
	 
	 
	 @Column(name = "is_delete", nullable = false)
	private boolean is_delete;
	 
	 @JsonIgnore
	 @OneToMany(fetch = FetchType.LAZY, mappedBy = "product")
	 private List<BillDetail> billDetails;
	 
	 @JsonIgnore
	 @OneToOne(fetch = FetchType.LAZY, mappedBy = "product")
	 private Pet pet;
	 
	 @JsonIgnore
	 @OneToOne(fetch = FetchType.LAZY, mappedBy = "product")
	 private SideDevice sideDevice;

	public Product() {
		super();
	}
// contructor for update
	public Product(Integer product_id, Integer category_id, String product_name, String product_image,
			double product_price, String product_description, boolean is_delete) {
		super();
		this.product_id = product_id;
		this.category_id = category_id;
		this.product_name = product_name;
		this.product_image = product_image;
		this.product_price = product_price;
		this.product_description = product_description;
		this.is_delete=is_delete;
	}
	
	//contructor for insert
	public Product(Integer category_id, String product_name, String product_image,
			double product_price, String product_description) {
		super();
		this.category_id = category_id;
		this.product_name = product_name;
		this.product_image = product_image;
		this.product_price = product_price;
		this.product_description = product_description;
		
	}
	
	public long getProduct_id() {
		return product_id;
	}

	public void setProduct_id(long product_id) {
		this.product_id = product_id;
	}

	public long getCategory_id() {
		return category_id;
	}

	public void setCategory_id(long category_id) {
		this.category_id = category_id;
	}

	public String getProduct_name() {
		return product_name;
	}

	public void setProduct_name(String product_name) {
		this.product_name = product_name;
	}

	public String getProduct_image() {
		return product_image;
	}

	public void setProduct_image(String product_image) {
		this.product_image = product_image;
	}

	public double getProduct_price() {
		return product_price;
	}

	public void setProduct_price(double product_price) {
		this.product_price = product_price;
	}

	public String getProduct_description() {
		return product_description;
	}

	public void setProduct_description(String product_description) {
		this.product_description = product_description;
	}

	public List<BillDetail> getBillDetails() {
		return billDetails;
	}

	public void setBillDetails(List<BillDetail> billDetails) {
		this.billDetails = billDetails;
	}

	public Pet getPet() {
		return pet;
	}

	public void setPet(Pet pet) {
		this.pet = pet;
	}

	public SideDevice getSideDevice() {
		return sideDevice;
	}

	public void setSideDevice(SideDevice sideDevice) {
		this.sideDevice = sideDevice;
	}

	public boolean isIs_delete() {
		return is_delete;
	}

	public void setIs_delete(boolean is_delete) {
		this.is_delete = is_delete;
	}
	
	
	 
	 
}
