package com.pnv.models;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "side_device"
)
public class SideDevice {

	@Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id_pro", unique = true, nullable = false)
	private long id_pro;
	
	@Column(name = "color", unique = true, nullable = false)
	private String color;
	
	@Column(name = "quantity", unique = true, nullable = false)
	private int quantity;
	
	@OneToOne(optional=false) 
    @JoinColumn(name="id_pro", nullable=false, updatable=false)
	@JsonIgnore
	private Product product;

	public SideDevice() {
		super();
	}

	public SideDevice(long id_pro, String color, int quantity) {
		super();
		this.id_pro = id_pro;
		this.color = color;
		this.quantity = quantity;
	}

	public long getId_pro() {
		return id_pro;
	}

	public void setId_pro(long id_pro) {
		this.id_pro = id_pro;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	
	
}
