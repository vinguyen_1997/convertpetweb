package com.pnv.api;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Base64;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import com.pnv.business.ProductBo;
import com.pnv.models.Product;
import com.pnv.models.ResponseStatus;

@Controller
@RequestMapping(value= "api/admin")
public class InsertApiController {
	@Autowired
	private ProductBo productBo;
	
	@Autowired
	private ResponseStatus responseStatus;
		
	@RequestMapping(value = "/insert_product", method = RequestMethod.POST)
	@ResponseBody
	   public ResponseStatus addOrEditDepartment(
	            @RequestParam("category_id") Integer category_id,
	            @RequestParam("product_name") String product_name,
	            @RequestParam("product_price") double product_price,
	            @RequestParam("product_description") String product_description,
	            @RequestParam("product_image") String product_image,
	   	HttpServletRequest request) {
	       
	   	Product p = new Product(category_id, product_name, product_description, product_price, product_image ); 
	     try { 
	   	 
	   	 
	   	 
	   	 if (!product_image.isEmpty()) {
	             HttpSession session = request.getSession();
	             ServletContext sc = session.getServletContext();
	             String imagePath = sc.getRealPath("/") + "resources/images/";

	             File fileSaveDir = new File(imagePath);

	                 // if the directory does not exist, create it
	             if (!fileSaveDir.exists()) {
	                 System.out.println("creating directory: " + imagePath);
	                 boolean isCreated = false;

	                 try {
	                 fileSaveDir.mkdir();
	                     isCreated = true;
	                 } catch (SecurityException se) {
	                     //handle it
	                 }
	                 if (isCreated) {
	                     System.out.println("DIR created");
	                 }
	             }
	             try 
	             {               
	                 byte[] scanBytes = Base64.getDecoder().decode(product_image);
	                 File uploadFile = new File( fileSaveDir.getAbsolutePath() + File.separator + product_image);
	                 BufferedOutputStream scanStream = new BufferedOutputStream( new FileOutputStream( uploadFile ) );       
	                 scanStream.write(scanBytes);
	                 scanStream.close();

	                 //"RegisterSuccessful";
	                 p.setProduct_image("images/"+ product_image);
	             }
	             catch (Exception e) 
	             {
	             System.out.println("You failed to upload scanImageFile => " + e.getMessage());
	               
	             }
	             
	          }
	   	 
	   	 
	     productBo.saveOrUpdate(p);
         responseStatus.setDataObject(p);
	      } catch (Exception ex) {
	   	  responseStatus.setStatus(0);
	   	  responseStatus.setMessage(ex.getMessage());
	   	  System.out.println(ex.getMessage()) ;
	   	  
	   	  return responseStatus;
	      } 
	      
	     responseStatus.setStatus(1);
	     return responseStatus;
	   }  

}
