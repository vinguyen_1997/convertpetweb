package com.pnv.api;

import java.io.IOException;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.pnv.business.UsersBO;
import com.pnv.models.ResponseStatus;
import com.pnv.models.User;
import com.pnv.utils.FormValidation;
import com.pnv.utils.MD5;



@Controller
@RequestMapping (value = "api/users")
public class RegisterApiController {
	@Autowired
	private UsersBO usersBO;

	@Autowired
	private ResponseStatus responseStatus;
	
	@RequestMapping(value = "/CheckEmailServlet", method = RequestMethod.POST)
	public void doCheckEmailServlet(HttpServletRequest request, HttpServletResponse response) throws IOException {
		request.setCharacterEncoding("utf-8");
		response.setCharacterEncoding("utf-8");
		if (usersBO.checkEmail(request.getParameter("email"))) {
			response.getWriter().write("<img src=\"resources/img/not-available.png\" />" + "<b> Email is Existed");
		} else {
			response.getWriter().write("<img src=\"resources/img/available.png\" />" + "<b> Can use");
		}
	}
	
	@RequestMapping(value = "/register", method = RequestMethod.POST)
	@ResponseBody
	public ResponseStatus addAccount(
			@RequestParam("email") String user_email, 
			@RequestParam("pass") String user_pass,
			HttpServletRequest request){
	HttpSession session = request.getSession();

	User users = new User();

	String user = request.getParameter("email");
	users.setUser_email(user);
	String pass = request.getParameter("pass");
	users.setUser_role(true);
	String errorUser = new FormValidation().emailInputValidation(user);
	String errorPass = new FormValidation().passwordValidation(pass);
	
	if (usersBO.checkEmail(request.getParameter("email")) || !errorUser.equals("") || !errorPass.equals("")) {
		session.setAttribute("error", "Error email or password!");
		responseStatus.setMessage("Not success!");
		responseStatus.setStatus(0);
	} else {
		String pw = MD5.encryption(pass);
		users.setUser_pass(pw);
		usersBO.insertUser(users);
		session.setAttribute("user", users);
		responseStatus.setDataObject(users);
		responseStatus.setMessage("Successfully!");
		responseStatus.setStatus(1);	
	}
	return responseStatus;
	}
	
}
