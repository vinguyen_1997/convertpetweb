package com.pnv.api;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.pnv.business.ProductBo;
import com.pnv.business.ProductBoImpl;
import com.pnv.dao.ProductDao;
import com.pnv.models.ResponseStatus;

@Controller
@RequestMapping(value = "api/admin")
public class ApiDeleteController {
	
	@Autowired
	private ProductBo productBo;
	@Autowired
	private ResponseStatus responseStatus;
	
	@RequestMapping(value = "/ApiDeleteControler", method = RequestMethod.POST)
	@ResponseBody	
    public ResponseStatus doDelete(@RequestParam("id") Integer id) {
    
    	try {
    		productBo.deleteProduct(id);
        } catch (Exception ex) {
        	responseStatus.setStatus(0);
        	responseStatus.setMessage(ex.getMessage());
     	   System.out.println(ex.getMessage()) ;
     	   
     	   return responseStatus;
        } 
    	responseStatus.setStatus(1);
        return responseStatus;
    }
}
