package com.pnv.api;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.pnv.business.UsersBO;
import com.pnv.business.UsersImplBO;
import com.pnv.models.ResponseStatus;
import com.pnv.models.User;
import com.pnv.utils.MD5;

@Controller
@RequestMapping(value = "api/users")
public class LogInApiController {

	@Autowired
	private UsersBO usersBO;

	@Autowired
	private ResponseStatus responseStatus;

	@RequestMapping(value = "/login", method = RequestMethod.POST)
	@ResponseBody
	public ResponseStatus logIn(@RequestParam("email") String email, @RequestParam("pass") String password,
			HttpServletRequest request) {

		HttpSession session = request.getSession();

		User users = new User();

		users = usersBO.login(email, MD5.encryption(password));
		if (users != null && users.getUser_role() == true) {
			session.setAttribute("user", users);
			responseStatus.setDataObject(users);
			responseStatus.setMessage("OK");
			responseStatus.setStatus(1);

		} else if (users != null && users.getUser_role() == false) {
			session.setAttribute("user", users);
			responseStatus.setDataObject(users);
			responseStatus.setStatus(1);

		} else {
			session.setAttribute("error", "Error email or password!");
			responseStatus.setStatus(0);
		}
		
		return responseStatus;
	}
}
