package com.pnv.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

import com.pnv.models.Product;
import com.pnv.business.ProductBo;

@Controller
public class ApiUserController {

@Autowired
private ProductBo productBo;

@RequestMapping(value = "api/ApiController", method = RequestMethod.GET)
   public  @ResponseBody List<Product> viewDepartmentPage() {
       /**
        * Get all titles
        */
       List<Product> product_list = productBo.showAllProduct();
       List<Product> list_product_no_delete = new ArrayList<Product>();
		for(Product product : product_list) {
			if(product.isIs_delete()) {
				// not do anything
			} else {
				list_product_no_delete.add(product);
			}
		}
		return list_product_no_delete;
//      return product_list;
   }
}
