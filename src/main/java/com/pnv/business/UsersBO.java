package com.pnv.business;

import com.pnv.models.User;

public interface UsersBO {
	public boolean checkEmail(String email);

	void insertUser(User u);

	public User login(String email, String password);

	/*public User getUser(long userID);
	
	public User adminLogin(String email, String password, boolean userRole);*/
		
	
}
