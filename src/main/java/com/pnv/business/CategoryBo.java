package com.pnv.business;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.pnv.dao.CategoryDAO;
import com.pnv.models.Category;

@Service
@Transactional
public class CategoryBo {

	@Autowired
	private CategoryDAO categoryDAO;
	
	public List<Category> showAllCategory() {
		
		return categoryDAO.getListCategory();
	}
	
}
