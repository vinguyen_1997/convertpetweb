package com.pnv.business;


import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

import org.apache.commons.collections.functors.ForClosure;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.pnv.dao.ProductDao;
import com.pnv.dao.SearchDao;
import com.pnv.models.Product;
@Service
@Transactional
public class ProductBoImpl implements ProductBo{

	@Autowired
	private ProductDao productDao;
	@Autowired
	private SearchDao searchDao;
	
	public List<Product> showAllProduct() {
		List<Product> list_product_no_delete = new ArrayList<Product>();
		for(Product product : productDao.getAllProduct()) {
			if(product.isIs_delete()) {
				// not do anything
			} else {
				list_product_no_delete.add(product);
			}
		}
		
		return list_product_no_delete;
		// return productDao.getAllProduct();
	}
	

	public void saveOrUpdate(Product product) {
		productDao.saveOrUpdate(product);
		
	}
	
	public Product getProduct(long productID){
		return this.productDao.getProduct(productID);
	}
	
	public List<Product> countProductByCategory(long categoryID) {
		return this.productDao.countProductByCategory(categoryID);
	}




	public List<Product> ShowProductByPage(int page, int total) {
		// TODO Auto-generated method stub
		return productDao.getListProductPage(page, total);
	}


	public boolean checkSearch(String conditionSearch) {
		return this.searchDao.checkSearch(conditionSearch);
	}


	public void deleteProduct(long product_id) {
		// TODO Auto-generated method stub
		productDao.deleteProduct(product_id);
	}
}
