package com.pnv.business;


import java.util.List;

import com.pnv.models.Product;

public interface ProductBo {
	public List<Product> showAllProduct();
	void saveOrUpdate(Product product);
	public Product getProduct(long productID);
	public List<Product> countProductByCategory(long categoryID);
	public List<Product> ShowProductByPage(int page, int total);
	public boolean checkSearch(String conditionSearch);
	public void deleteProduct(long product_id);
}
