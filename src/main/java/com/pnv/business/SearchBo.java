package com.pnv.business;

import java.util.List;

import com.pnv.models.Product;

public interface SearchBo {
	public List<Product> showProductInRangeOfPrice(int price1, int price2);
	public List<Product> showProductsWithSearchCondition(String conditionSearch);

}
