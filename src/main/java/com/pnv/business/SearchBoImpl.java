package com.pnv.business;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.pnv.dao.SearchDao;
import com.pnv.models.Product;

@Service
@Transactional
public class SearchBoImpl implements SearchBo{
	@Autowired
	private SearchDao searchDAO;
	public List<Product> showProductInRangeOfPrice(int price1, int price2) {
		
		List<Product> list_product_no_delete = new ArrayList<Product>();
		for(Product product : this.searchDAO.getProductsInRangeOfPrice(price1, price2)) {
			if(product.isIs_delete()) {
				// not do anything
			} else {
				list_product_no_delete.add(product);
			}
		}
		return list_product_no_delete;
		
		
//		return this.searchDAO.getProductsInRangeOfPrice(price1, price2);
	}

	public List<Product> showProductsWithSearchCondition(String conditionSearch) {
		List<Product> list_product_no_delete = new ArrayList<Product>();
		for(Product product : this.searchDAO.showProductsWithSearchCondition(conditionSearch)) {
			if(product.isIs_delete()) {
				// not do anything
			} else {
				list_product_no_delete.add(product);
			}
		}
		return list_product_no_delete;
		
//		return this.searchDAO.showProductsWithSearchCondition(conditionSearch);
	}

}
