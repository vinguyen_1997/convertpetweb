package com.pnv.business;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.pnv.dao.BillDAO;
import com.pnv.models.Bill;


@Service
@Transactional
public class BillImplBO implements BillBO{
	
	@Autowired
	private BillDAO billDao;
	

/*	public void addBill(Bill bill) {
		billDao.insertBill(bill);

	}
*/
	public List<Bill> showListBill() {
		return billDao.getListBill();
	}
	
}

