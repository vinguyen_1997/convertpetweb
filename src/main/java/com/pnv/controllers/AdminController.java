package com.pnv.controllers;


import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.pnv.business.BillBO;
import com.pnv.business.CategoryBo;
import com.pnv.business.ProductBo;
import com.pnv.dao.ChartDao;
import com.pnv.dao.ChartDaoImpl;
import com.pnv.dao.ProductDao;
import com.pnv.models.Product;



/**
 *
 * @author Administrator
 */
@Controller
public class AdminController {

	@Autowired
	 private ProductBo productBo;
	
	@Autowired
	private CategoryBo categoryBo;
	
	@Autowired
	private BillBO billBo;
	@Autowired
	private ChartDao chartDao;
	
    @RequestMapping(value = "/admin", method = RequestMethod.GET)
    public String viewAdminPage() {
        return "admin/index";
    }

    
    @RequestMapping(value = "/admin/manager_product", method = RequestMethod.GET)
    public String viewProductPage(ModelMap map) {
    	map.addAttribute("products", productBo.showAllProduct());

        return "admin/manager_product";
    }
    
    

    
    // delete
    @RequestMapping(value ="/admin/manager_product/delete", method = RequestMethod.POST)
    public String deleteProduct(ModelMap map, HttpServletRequest request) {
    	
    	//HttpSession session = request.getSession();
    	int id = Integer.parseInt(request.getParameter("id")) ;
    	productBo.deleteProduct(id);
    	//map.addAttribute("products", productBo.showAllProduct());
    	return "admin/index";
    }
   
    
    
    @RequestMapping(value = "/admin/insert_product", method = RequestMethod.GET)
    public String viewAddNewPage(ModelMap map) {
    	Product productForm = new Product();
        map.addAttribute("productForm", productForm);
        return "admin/insert_product";
        

    }

    @RequestMapping(value = "/admin/insert_product", method = RequestMethod.POST)
    public String doAddNew(@Valid @ModelAttribute("productForm") Product productForm,
            BindingResult result, ModelMap map, @RequestParam("file") MultipartFile file, HttpServletRequest request, HttpServletResponse response) throws IOException {
        if (!file.isEmpty()) {
            HttpSession session = request.getSession();
            ServletContext sc = session.getServletContext();
            String imagePath = sc.getRealPath("/") + "resources/images/";

            File theDir = new File(imagePath);

                // if the directory does not exist, create it
            if (!theDir.exists()) {
                System.out.println("creating directory: " + imagePath);
                boolean isCreated = false;

                try {
                    theDir.mkdir();
                    isCreated = true;
                } catch (SecurityException se) {
                    //handle it
                }
                if (isCreated) {
                    System.out.println("DIR created");
                }
            }
            InputStream inputStream = null;
            OutputStream outputStream = null;
            if (file.getSize() > 0) {
                inputStream = file.getInputStream();
                File newFile = new File(imagePath + file.getOriginalFilename());
                if (!newFile.exists()) {
                    newFile.createNewFile();
                }
                // File realUpload = new File("C:/");
                outputStream = new FileOutputStream(imagePath
                        + file.getOriginalFilename());
                int readBytes = 0;
                byte[] buffer = new byte[8192];
                while ((readBytes = inputStream.read(buffer, 0, 8192)) != -1) {
                    outputStream.write(buffer, 0, readBytes);
                }
                outputStream.close();
                inputStream.close();
                
                /**
                 * 
                    set image to model
                */
                productForm.setProduct_image("images/"+file.getOriginalFilename());
            }
        }
        productBo.saveOrUpdate(productForm);

        /**
         * Get all titles
         */

        response.sendRedirect("/petOnline/admin/manager_product");
		return "admin/manager_product";
    }
    
    @RequestMapping(value = "/admin/manager_category", method = RequestMethod.GET)
    public String viewCategoryPage(ModelMap map) {
    	map.addAttribute("categorys", categoryBo.showAllCategory());
        return "admin/manager_category";
    }
    
	@RequestMapping(value = "/admin/manager_bill", method = RequestMethod.GET)
	public String viewBillPage(ModelMap map){
		map.addAttribute("bill", billBo.showListBill());
		return "admin/manager_bill";
	}

	@RequestMapping(value = "/admin/manager_chart", method = RequestMethod.GET)
	
	public String viewChart(ModelMap map, HttpServletRequest request ){
		request.setAttribute("listItem", chartDao.getAll());
		return "admin/manager_chart";
	}
	
	@RequestMapping(value = "/admin/edit", method = RequestMethod.GET)
    public String viewEditTitlePage(@RequestParam(value = "id", required = true) int id, ModelMap map) {

        map.addAttribute("productForm", productBo.getProduct(id));
        return "admin/insert_product";
    }
}