/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pnv.controllers;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.mysql.jdbc.StringUtils;
import com.pnv.business.CategoryBo;

import com.pnv.business.ProductBo;
import com.pnv.business.SearchBo;
import com.pnv.business.UsersBO;
import com.pnv.models.Cart;
import com.pnv.models.Item;
import com.pnv.models.Product;
import com.pnv.models.User;
import com.pnv.utils.FormValidation;
import com.pnv.utils.MD5;

/**
 *
 * @author Administrator
 */
@Controller
public class UserController {

	@Autowired
	private ProductBo productBo;
	@Autowired
	private CategoryBo categoryBo;
	@Autowired
	private UsersBO usersBO;
	@Autowired
	private SearchBo searchBO;

	private String value1 = "Under 100.000";
	private String value2 = "100.000-300.000";
	private String value3 = "300.000-500.000";
	private String value4 = "More 500.000";
	private int price1 = 0;
	private int price2 = 100000;
	private int price3 = 300000;
	private int price4 = 500000;
	private int price5 = 10000000;

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String viewWelcomePage(ModelMap map) {
		map.addAttribute("products", productBo.showAllProduct());
		map.addAttribute("categorys", categoryBo.showAllCategory());
		return "users/index";
	}

	@RequestMapping(value = "/login", method = RequestMethod.GET)

	public void viewBillPage(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		session.setAttribute("error", "");

		User users = (User) session.getAttribute("user");
		if (users == null) {
			request.getRequestDispatcher("WEB-INF/jsp/users/login.jsp").forward(request, response);
		} else {
			request.getRequestDispatcher("").forward(request, response);
		}
	}

	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public void loginPage(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		HttpSession session = request.getSession();

		User users = new User();

		users = usersBO.login(request.getParameter("email"), MD5.encryption(request.getParameter("pass")));
		if (users != null && users.getUser_role() == true) {
			session.setAttribute("user", users);
			request.getRequestDispatcher("WEB-INF/jsp/users/index.jsp").forward(request, response);
		} else if (users != null && users.getUser_role() == false) {
			session.setAttribute("user", users);
			request.getRequestDispatcher("WEB-INF/jsp/admin/index.jsp").forward(request, response);

		} else {
			session.setAttribute("error", "Error email or password!");
			request.getRequestDispatcher("WEB-INF/jsp/users/login.jsp").forward(request, response);
		}

	}

	@RequestMapping(value = "/logout", method = RequestMethod.GET)
	public void logOutPage(HttpServletRequest request, HttpServletResponse response) throws IOException {
		HttpSession session = request.getSession();
		session.invalidate();
		response.sendRedirect("/petOnline/");

	}

	@RequestMapping(value = "/register", method = RequestMethod.GET)
	public void viewRegisterPage(ModelMap map, HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		User users = (User) session.getAttribute("user");
		session.setAttribute("error", "");
		if (users == null) {
			request.getRequestDispatcher("WEB-INF/jsp/users/register.jsp").forward(request, response);
		} else {
			request.getRequestDispatcher("").forward(request, response);
		}
	}

	@RequestMapping(value = "/register", method = RequestMethod.POST)
	public void registerPage(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		HttpSession session = request.getSession();

		User users = new User();

		String user = request.getParameter("email");
		users.setUser_email(user);
		String pass = request.getParameter("pass");
		users.setUser_role(true);
		String errorUser = new FormValidation().emailInputValidation(user);
		String errorPass = new FormValidation().passwordValidation(pass);
		if (usersBO.checkEmail(request.getParameter("email")) || !errorUser.equals("") || !errorPass.equals("")) {
			session.setAttribute("error", "Error email or password!");
			request.getRequestDispatcher("WEB-INF/jsp/users/register.jsp").forward(request, response);

		} else {
			String pw = MD5.encryption(pass);
			users.setUser_pass(pw);
			usersBO.insertUser(users);
			session.setAttribute("user", users);
			response.sendRedirect("/petOnline");
		}

	}

	@RequestMapping(value = "/CheckEmailServlet", method = RequestMethod.POST)
	public void doCheckEmailServlet(HttpServletRequest request, HttpServletResponse response) throws IOException {
		request.setCharacterEncoding("utf-8");
		response.setCharacterEncoding("utf-8");
		if (usersBO.checkEmail(request.getParameter("username"))) {
			response.getWriter().write("<img src=\"resources/img/not-available.png\" />" + "<b> Email is Existed");
		} else {
			response.getWriter().write("<img src=\"resources/img/available.png\" />" + "<b> Can use");
		}
	}

	@RequestMapping(value = "/CartServlet/plus/{id}", method = { RequestMethod.GET })
	public @ResponseBody void viewCartPage(@PathVariable(value = "id") Long id, ModelMap map,
			HttpServletRequest request, HttpServletResponse response) throws IOException {
		HttpSession session = request.getSession();

		Cart cart = (Cart) session.getAttribute("cart");
		try {

			Product product = productBo.getProduct(id);

			if (cart.getCartItems().containsKey(id)) {

				cart.plusToCart(id, new Item(product, cart.getCartItems().get(id).getQuantity()));
			} else {
				cart.plusToCart(id, new Item(product, 1));
			}

		} catch (Exception e) {
			e.printStackTrace();
			// response.sendRedirect("/PetWeb/");
		}

		session.setAttribute("cart", cart);
		response.sendRedirect("/petOnline/");
	}

	@RequestMapping(value = "/CartServlet/remove/{id}", method = { RequestMethod.GET })
	public @ResponseBody void removeCart(@PathVariable(value = "id") Long id, ModelMap map, HttpServletRequest request,
			HttpServletResponse response) throws SQLException, IOException {
		HttpSession session = request.getSession();
		Cart cart = (Cart) session.getAttribute("cart");
		cart.removeToCart(id);
		session.setAttribute("cart", cart);
		response.sendRedirect("/petOnline/");
	}

	@RequestMapping(value = "/single/{id}", method = RequestMethod.GET)
	public String viewSinglePage(@PathVariable(value = "id") Long id, ModelMap map) {

		Product product = productBo.getProduct(id);
		map.addAttribute("product", product);
		
		return "users/single";
	}

	@RequestMapping(value = "/product/{id}", method = RequestMethod.GET)
	public String viewProductPage(@PathVariable(value = "id") Long id, ModelMap map) {
		List<Product> product = productBo.countProductByCategory(id);
		map.addAttribute("products", product);
		map.addAttribute("categorys", categoryBo.showAllCategory());
		return "users/product";
	}

	@RequestMapping(value = "/list/{pageid}")
	public String showPageProduct(@PathVariable(value = "pageid") int pageid, ModelMap map) {
		int total = 8;
		if (pageid == 1) {
		} else {
			pageid = (pageid - 1) * total + 1;
		}
		List<Product> list = productBo.ShowProductByPage(pageid, total);
		map.addAttribute("products", list);
		return "users/product";
	}

	@RequestMapping(value = "/side", method = RequestMethod.GET)
	public String viewSearchProduct(ModelMap map, HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		response.setContentType("text/html");
		request.setCharacterEncoding("utf-8");
		String conditionSearch = request.getParameter("Price");

		List<Product> products = null;

		if (conditionSearch.equals(value1)) {
			products = searchBO.showProductInRangeOfPrice(price1, price2);
		} else if (conditionSearch.equals(value2)) {
			products = searchBO.showProductInRangeOfPrice(price2, price3);
		} else if (conditionSearch.equals(value3)) {
			products = searchBO.showProductInRangeOfPrice(price3, price4);
		} else if (conditionSearch.equals(value4)) {
			products = searchBO.showProductInRangeOfPrice(price4, price5);
		}
		request.setAttribute("products", products);

		return "users/result";
	}

	@RequestMapping(value = "/side", method = RequestMethod.POST)
	public String doSearchProduct(ModelMap map, HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		response.setContentType("text/html");
		request.setCharacterEncoding("utf-8");

		String conditionSearch = request.getParameter("Search");
		if (StringUtils.isNullOrEmpty(conditionSearch)) {
			request.setAttribute("MS", "Sorry! You don't input data.");
			return "users/message";
		} else if (productBo.checkSearch(conditionSearch)) {
			List<Product> products = searchBO.showProductsWithSearchCondition(conditionSearch);
			request.setAttribute("products", products);

			return "users/result";
		} else {
			request.setAttribute("MS", "Sorry! Product not found " + "'" + conditionSearch + "'");

			return "users/message";
		}
	}

	@RequestMapping(value = "/contact", method = RequestMethod.GET)
	public String viewContactPage(ModelMap map) {
		return "users/contact";
	}

}
