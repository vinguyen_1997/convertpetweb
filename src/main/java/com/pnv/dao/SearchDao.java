package com.pnv.dao;

import java.util.List;

import com.pnv.models.Product;

public interface SearchDao {
	public List<Product> getProductsInRangeOfPrice(int price1, int price2);
	public List<Product> showProductsWithSearchCondition(String conditionSearch);
	public boolean checkSearch(String conditionSearch);

}
