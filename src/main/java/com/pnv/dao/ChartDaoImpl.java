package com.pnv.dao;

import java.util.ArrayList;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.pnv.models.Category;

@Service
@Transactional
public class ChartDaoImpl implements ChartDao{

	@Autowired
	private SessionFactory sessionFactory;
	public ArrayList<Category> getAll() {
		ArrayList<Category> list =  (ArrayList<Category>) sessionFactory.getCurrentSession().createQuery("from Category").list();
        return list;
	}

}
