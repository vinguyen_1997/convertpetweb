/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pnv.dao;


import java.util.List;

import com.pnv.models.Bill;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
@Transactional
public class BillDAOImpl implements BillDAO{

	@Autowired
	private SessionFactory sessionFactory;
	
/*    public void insertBill(Bill bill){
    	
    	sessionFactory.getCurrentSession().saveOrUpdate(bill);
    	
    }*/
    
    public List<Bill> getListBill() {
		List<Bill> bill = sessionFactory.getCurrentSession().createQuery("from Bill").list();
		return bill;
    }

}
