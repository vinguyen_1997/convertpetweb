package com.pnv.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.pnv.models.Product;

@Service
@Transactional
public class SearchDaoImpl implements SearchDao{
	@Autowired
	private SessionFactory sessionFactory;

	public List<Product> getProductsInRangeOfPrice(int price1, int price2) {
		List<Product> products;
		Query query = (Query) sessionFactory.getCurrentSession()
				.createQuery("from Product AS P WHERE P.product_price BETWEEN " + price1 + " AND " + price2);

		products = (List<Product>) query.list();
		return products;

	}

	public List<Product> showProductsWithSearchCondition(String conditionSearch) {
		List<Product> products;
		Query query = (Query) sessionFactory.getCurrentSession()
				.createQuery("from Product AS P WHERE P.product_name LIKE '%" + conditionSearch + "%'");

		products = (List<Product>) query.list();
		return products;

	}

	public boolean checkSearch(String conditionSearch) {
		List<Product> products = sessionFactory.getCurrentSession()
				.createQuery("from Product AS P WHERE P.product_name like '%" + conditionSearch + "%'").list();
		if (products.size() >= 1) {
			return true;
		} else {
			return false;
		}
	}

}
