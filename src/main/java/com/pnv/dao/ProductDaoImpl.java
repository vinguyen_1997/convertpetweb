package com.pnv.dao;


import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.pnv.models.Product;


@Service
@Transactional
public class ProductDaoImpl implements ProductDao {

	@Autowired
	private SessionFactory sessionFactory;
	public List<Product> getAllProduct() {
		List<Product> products = sessionFactory.getCurrentSession().createQuery("from Product").list();
		return products;
	}

	public void saveOrUpdate(Product product) {
		sessionFactory.getCurrentSession().saveOrUpdate(product);

	}

	public Product getProduct(long productID) {
		String StrQuery = "from Product WHERE product_id = :productid";
		Query query = sessionFactory.getCurrentSession().createQuery(StrQuery);
		query.setParameter("productid", productID);
		Product product = (Product) query.uniqueResult();
		return product;
	}

	public List<Product> countProductByCategory(long categoryID) {
		String StrQuery = "FROM Product WHERE category_id = :categoryid";
		Query query = sessionFactory.getCurrentSession().createQuery(StrQuery);
		query.setParameter("categoryid", categoryID);

		List<Product> product = (List<Product>) query.list();
		return product;
	}

	public List<Product> getListProductByCategory(long category_id) {
		String StrQuery = "from Product WHERE category_id = :categoryid";
		Query query = sessionFactory.getCurrentSession().createQuery(StrQuery);
		query.setParameter("categoryid", category_id);
		List<Product> product = (List<Product>) query.list();
		return product;
	}

	public List<Product> getListProductPage(int page, int total) {
        Query q = sessionFactory.getCurrentSession().createQuery("from Product");
        q.setFirstResult(page); 
        q.setMaxResults(total);
        return (List<Product>) q.list();
	}

	public void deleteProduct(long productID) {
//		String StrQuery = "delete Product WHERE product_id = :ID";
//		Query query = sessionFactory.getCurrentSession().createQuery(StrQuery);
//		query.setParameter("ID", productID);
//		query.executeUpdate();
		
		String StrQuery = "UPDATE Product SET is_delete = True WHERE product_id = :ID";
		Query query = sessionFactory.getCurrentSession().createQuery(StrQuery);
		query.setParameter("ID", productID);
		query.executeUpdate();
		 
	}
		
	}


