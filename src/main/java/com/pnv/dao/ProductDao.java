package com.pnv.dao;


import java.util.List;

import com.pnv.models.Product;

public interface ProductDao {
	public List<Product> getAllProduct();
	void saveOrUpdate(Product product);
	public Product getProduct(long productID);
	public List<Product> countProductByCategory(long categoryID);
	public List<Product> getListProductByCategory(long category_id);
	public List<Product> getListProductPage(int page, int total);
	public void deleteProduct(long productID);
}
