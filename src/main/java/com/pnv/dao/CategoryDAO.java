package com.pnv.dao;


import java.util.List;


import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.pnv.models.Category;

@Service
@Transactional

public class CategoryDAO {

	@Autowired
	private SessionFactory sessionFactory;
	
	public List<Category> getListCategory(){
		
		List<Category> category = sessionFactory.getCurrentSession().createQuery("from Category").list();
		return category;
		
	}

	/*public boolean insertCategory(Category c) {
		Connection connection = DBConnect.getConnection();
		String sql = "INSERT INTO category VALUES(?,?)";
		try {
			PreparedStatement ps = connection.prepareCall(sql);
			ps.setLong(1, c.getCategoryID());
			ps.setString(2, c.getCategoryName());
			return ps.executeUpdate() == 1;
		} catch (SQLException ex) {
			Logger.getLogger(CategoryDAO.class.getName()).log(Level.SEVERE, null, ex);
		}
		return false;
	}

	public boolean updateCategory(Category c) {
		Connection connection = DBConnect.getConnection();
		String sql = "UPDATE category SET category_name = ? WHERE category_id = ?";
		try {
			PreparedStatement ps = connection.prepareCall(sql);
			ps.setString(1, c.getCategoryName());
			ps.setLong(2, c.getCategoryID());
			return ps.executeUpdate() == 1;
		} catch (SQLException ex) {
			Logger.getLogger(CategoryDAO.class.getName()).log(Level.SEVERE, null, ex);
		}
		return false;
	}

	public boolean deleteCategory(long category_id) {
		Connection connection = DBConnect.getConnection();
		String sql = "DELETE FROM category WHERE category_id = ?";
		try {
			PreparedStatement ps = connection.prepareCall(sql);
			ps.setLong(1, category_id);
			return ps.executeUpdate() == 1;
		} catch (SQLException ex) {
			Logger.getLogger(CategoryDAO.class.getName()).log(Level.SEVERE, null, ex);
		}
		return false;
	}

	public static void main(String[] args) throws SQLException {
		CategoryDAO dao = new CategoryDAO();

		System.out.println(dao.deleteCategory(7));
	}
*/
}
