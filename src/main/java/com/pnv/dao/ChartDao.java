package com.pnv.dao;

import java.util.ArrayList;

import com.pnv.models.Category;

public interface ChartDao {
	public ArrayList<Category> getAll();
}
