package com.pnv.dao;

import com.pnv.models.User;

public interface UsersDAO {
	public boolean checkEmail(String email);
	
	public void insertUser(User u);
	
	public User login(String email, String password);
	
/*	public User getUser(long userID);*/
	/*
	public User adminLogin(String email, String password, boolean userRole);
	*/
	
}
