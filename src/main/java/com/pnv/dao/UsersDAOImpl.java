package com.pnv.dao;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import org.hibernate.SessionFactory;
import com.pnv.dao.UsersDAO;

import org.hibernate.Query;

import com.pnv.models.User;

@Service
@Transactional
public class UsersDAOImpl implements UsersDAO {

	@Autowired
	private SessionFactory sessionFactory;

	public boolean checkEmail(String email) {

		String StrQuery = "from User WHERE user_email = :username";
		Query query = sessionFactory.getCurrentSession().createQuery(StrQuery);
		query.setParameter("username", email);
		User user = (User) query.uniqueResult();

		if (user != null) {
			return true;
		}

		return false;
	}

	public void insertUser(User u) {
		sessionFactory.getCurrentSession().saveOrUpdate(u);

	}

	public User login(String email, String password) {

		String StrQuery = "from User WHERE user_email = :username AND user_pass = :password";
		Query query = sessionFactory.getCurrentSession().createQuery(StrQuery);
		query.setParameter("username", email);
		query.setParameter("password", password);

		User user = (User) query.uniqueResult();
		if (user != null) {
			return user;
		}
		return null;

	}

	/*
	 * public Users adminLogin(String email, String password, boolean userRole){
	 * Connection connection = DBConnect.getConnection(); sql=
	 * "SELECT * FROM users WHERE user_email='" + email + "' AND user_pass='" +
	 * password + "' AND user_role= '0'"; try { ps=
	 * connection.prepareStatement(sql); rs = ps.executeQuery(); if (rs.next())
	 * { Users u = new Users(); u.setUserID(rs.getLong("user_id"));
	 * u.setUserEmail(rs.getString("user_email"));
	 * u.setUserPass(rs.getString("user_pass"));
	 * u.setUserRole(rs.getBoolean("user_role")); connection.close(); return u;
	 * } } catch (Exception e) { e.printStackTrace(); } return null;
	 */

/*	public User getUser(long userID) {
		String StrQuery = "from User WHERE user_id = :userid";
		Query query = sessionFactory.getCurrentSession().createQuery(StrQuery);
		query.setParameter("userid", userID);
		User user = (User) query.uniqueResult();
		user.setUser_email(user.getUser_email());
		return user;

	}*/
}
