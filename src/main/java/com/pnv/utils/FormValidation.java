package com.pnv.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FormValidation {
	public static String emailInputValidation(String email) {
		String errorMessage = "";
		if (email.equals("")) {
			errorMessage = "Mail can not be blank!";
		
		} else {
			Pattern pattern = Pattern.compile(new String("^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$"));
			Matcher matcher = pattern.matcher(email);
		
			if (!matcher.matches()) {
				errorMessage = "Email is invalid!";
				
			}
		}
		System.out.println("4");
		return errorMessage;
	}
	public static String passwordValidation(String input) {
		String errorMessage = "";
		if (input.equals("")) {
			errorMessage = "Password can not be blank!";
		}else {
			Pattern pattern = Pattern.compile(new String(".{3,8}"));
			Matcher matcher = pattern.matcher(input);
			if (!matcher.matches()) {
				errorMessage = "Password is invalid!";
			}
		}
		return errorMessage;
	
}

}
