<%@page import="com.fasterxml.jackson.annotation.JsonInclude.Include"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@include file="include.jsp"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Thêm sản phẩm</title>

<c:set var="root" value="${pageContext.request.contextPath}" />
<link href="${root}/resources/css/mos-style.css" rel='stylesheet'
	type='text/css' />
<script src="<c:url value="/ckeditor/ckeditor.js" />"></script>

</head>
<body>

	<jsp:include page="header.jsp"></jsp:include>

	<div id="wrapper">

		<jsp:include page="menu.jsp"></jsp:include>

		<div id="rightContent">
			<h3>Thông tin sản phẩm</h3>

			<table width="95%">
				<form:form action="insert_product" modelAttribute="productForm"
					method="POST" enctype="multipart/form-data">
					<form:hidden path="product_id"></form:hidden>
					<tr>
						<td><b>Categery</b></td>
						<td><form:select path="category_id">
								<form:option value="1">Pet</form:option>
								<form:option value="2">Side device</form:option>
							</form:select></td>
					</tr>
					<tr>
						<td><b>Tên sản phẩm</b></td>
						<td><form:input type="text" class="sedang"
								path="product_name" /></td>
						<td><form:errors path="product_name" cssClass="error" /></td>
					</tr>

					<tr>
						<td><b>Giá bán</b></td>
						<td><form:input type="text" class="sedang" path="product_price" /></td>
						<td><form:errors path="product_price" cssClass="error" /></td>
					</tr>

					<tr>
						<td><b>Mô tả sản phẩm</b></td>
						<td><form:textarea class="form-textarea" id="noiDung"
								path="product_description"></form:textarea> <script
								type="text/javascript" language="javascript">
									CKEDITOR.replace('noiDung', {
										width : '500px',
										height : '300px'
									});
								</script></td>
						<td><form:errors path="product_description" cssClass="error" /></td>
					</tr>
					<tr>
						<td><b>Hình ảnh</b></td>
						<td><input type="file" name="file"></td>
					</tr>
					<tr>
						<td></td>
						<td><input type="button" class="button" value="Button">
							<input type="submit" class="button" value="Submit"> <input
							type="reset" class="button" value="Reset"></td>
					</tr>
				</form:form>
			</table>

		</div>
		<div class="clear"></div>

		<jsp:include page="footer.jsp"></jsp:include>

	</div>


</body>
</html>
