<%@page import="com.pnv.models.Category"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.pnv.dao.CategoryDAO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Quản lý danh mục</title>

        <c:set var="root" value="${pageContext.request.contextPath}"/>
        <link href="${root}/resources/css/mos-style.css" rel='stylesheet' type='text/css' />

    </head>
    <body>



        <jsp:include page="header.jsp"></jsp:include>

            <div id="wrapper">

            <jsp:include page="menu.jsp"></jsp:include>

                <div id="rightContent">
                    <h3>Quản lý danh mục</h3>
                    
                    <a href="${root}/admin/insert_category">Thêm danh mục</a>

                    <table class="data">

                        <tr class="data">
                            <th class="data" width="30px">STT</th>
                            <th class="data">Mã danh mục</th>
                            <th class="data">Tên danh mục</th>
                            <th class="data" width="90px">Tùy chọn</th>
                        </tr>

                    <c:forEach var="category" items="${categorys}">
                        <tr class="data">
                            <td class="data" width="30px">${category.category_id}</td>
                            <td class="data">${category.category_id}</td>
                            <td class="data">${category.category_name}</td>
                            <td class="data" width="90px">
                            <center>
                                <a href="${root}/update?command=update&category_id=${category.category_id}">Sửa</a>&nbsp;&nbsp; | &nbsp;&nbsp;
                                <a href="${root}/ManagerCategoryServlet?command=delete&category_id=${category.category_id}">Xóa</a>
                            </center>
                            </td>
                        </tr>
                        </c:forEach>

                    </table>
                </div>
                <div class="clear"></div>

            <jsp:include page="footer.jsp"></jsp:include>

        </div>

    </body>
</html>
