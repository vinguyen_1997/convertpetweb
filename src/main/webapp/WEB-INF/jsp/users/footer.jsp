<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>footer</title>
<c:set var="root" value="${pageContext.request.contextPath}"/>
<link href="${root}/resources/css/bootstrap.css" rel="stylesheet" type="text/css"
	media="all" />
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="${root}/resources/js/jquery.min.js"></script>
<!-- Custom Theme files -->
<!--theme-style-->
<link href="${root}/resources/css/style.css" rel="stylesheet" type="text/css" media="all" />
<!--//theme-style-->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords"
	content="Bonfire Responsive web template, Bootstrap Web Templates, Flat Web Templates, Andriod Compatible web template, 
              Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyErricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!--fonts-->
<link
	href='http://fonts.googleapis.com/css?family=Exo:100,200,300,400,500,600,700,800,900'
	rel='stylesheet' type='text/css'>
<!--//fonts-->
<script type="text/javascript" src="${root}/resources/js/move-top.js"></script>
<script type="text/javascript" src="${root}/resources/js/easing.js"></script>
<script type="text/javascript">
            jQuery(document).ready(function ($) {
                $(".scroll").click(function (event) {
                    event.preventDefault();
                    $('html,body').animate({scrollTop: $(this.hash).offset().top}, 1000);
                });
            });
        </script>
<!--slider-script-->
<script src="${root}/resources/js/responsiveslides.min.js"></script>
<script>
                    $(function () {
                        $("#slider1").responsiveSlides({
                            auto: true,
                            speed: 500,
                            namespace: "callbacks",
                            pager: true,
                        });
                    });
        </script>
<!--//slider-script-->
<script>$(document).ready(function (c) {
                $('.alert-close').on('click', function (c) {
                    $('.message').fadeOut('slow', function (c) {
                        $('.message').remove();
                    });
                });
            });
        </script>
<script>$(document).ready(function (c) {
                $('.alert-close1').on('click', function (c) {
                    $('.message1').fadeOut('slow', function (c) {
                        $('.message1').remove();
                    });
                });
            });
        </script>
</head>
<body>

	<!---->
	<div class="footer">
		<div class="footer-top">
			<div class="container">
				<div class="col-md-4 footer-in">
					<h4>
						<i> </i>Mẹo Chăm Sóc Pet
					</h4>
					<p>Có chế độ ăn uống hợp lý, luôn theo dõi tình hình của thú
						cưng và đặc biệt phải vệ sinh cho thú cưng của bạn</p>
				</div>
				<div class="col-md-4 footer-in">
					<h4>
						<i class="cross"> </i>Những Điều Lưu Ý
					</h4>
					<p>Bạn phải hiểu tập tính của vật nuôi, để tránh những trường
						hợp đáng tiếc xãy ra vì bị thú cưng cắn hay gây bệnh</p>
				</div>
				<div class="col-md-4 footer-in">
					<h4>
						<i class="down"> </i>Lời Khuyên
					</h4>
					<p>Việc nuôi thú cưng đem lại rất nhiều lợi ích: Giúp bạn có
						thêm một người bạn, giảm stress, tự tin hơn...</p>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
		<!---->
		<div class="footer-middle">
			<div class="container">
				<div class="footer-middle-in">
					<h6>Giới thiệu</h6>
					<p>Chúng tôi gồm có 5 thành viên, đến từ lớp PNV17DEV, sinh
						viên năm 2 học tại tổ chức Passereles numeriques Việt Nam</p>
				</div>
				<div class="footer-middle-in">
					<h6>Danh sách thành viên</h6>
					<ul>
						<li><a href="#">Nguyễn Công Danh(leader)</a></li>
						<li><a href="#">Lê Thị Bích Viển</a></li>
						<li><a href="#">Lê Thị Thường</a></li>
						<li><a href="#">Trần Thị Thành</a></li>
						<li><a href="#">Nguyễn Thị Thùy Vi</a></li>
					</ul>
				</div>
				<div class="footer-middle-in">
					<h6>giá trị của shop</h6>
					<ul>
						<li><a href="#">Tin Tưởng</a></li>
						<li><a href="#">Trách nhiệm</a></li>
						<li><a href="#">Đoàn kết</a></li>
					</ul>
				</div>
				<div class="footer-middle-in">
					<h6>Liên hệ</h6>
					<ul>
						<li><a href="#">096425207</a></li>
						<li><a href="#">Sky: PNV - Pet's Shop</a></li>
						<li><a href="#">danh.nguyen@gmail.com</a></li>
						<li><a href="#">80B Lê Duẩn</a></li>
					</ul>
				</div>
				<div class="footer-middle-in">
					<h6>Cam kết</h6>
					<ul>
						<li><a href="#">Vui vẻ</a></li>
						<li><a href="#">Lịch sự</a></li>
						<li><a href="#">Chu đáo</a></li>
						<li><a href="#">Tận tình</a></li>
					</ul>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
		<p class="footer-class">
			KITES TEAM<a href="#" target="_blank"> 5
				MEMBERS</a>
		</p>
		<script type="text/javascript">
                $(document).ready(function () {
                    /*
                     var defaults = {
                     containerID: 'toTop', // fading element id
                     containerHoverID: 'toTopHover', // fading element hover id
                     scrollSpeed: 1200,
                     easingType: 'linear' 
                     };
                     */

                    $().UItoTop({easingType: 'easeOutQuart'});

                });
            </script>
		<a href="#" id="toTop" style="display: block;"> <span
			id="toTopHover" style="opacity: 1;"> </span></a>

	</div>


	<script type='text/javascript'>window._sbzq||function(e){e._sbzq=[];var t=e._sbzq;t.push(["_setAccount",42539]);var n=e.location.protocol=="https:"?"https:":"http:";var r=document.createElement("script");r.type="text/javascript";r.async=true;r.src=n+"//static.subiz.com/public/js/loader.js";var i=document.getElementsByTagName("script")[0];i.parentNode.insertBefore(r,i)}(window);</script>

</body>
</html>
