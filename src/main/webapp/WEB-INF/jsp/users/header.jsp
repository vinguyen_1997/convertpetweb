<%@page import="com.pnv.models.Item"%>
<%@page import="java.util.Map"%>
<%@page import="com.pnv.models.Cart"%>
<%@page import="com.pnv.models.User"%>
<%@page import="com.pnv.models.Category"%>
<%@page import="com.pnv.dao.CategoryDAO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>header</title>
<c:set var="root" value="${pageContext.request.contextPath}"/>
<div id="fb-root"></div>
<script>
	(function(d, s, id) {
		var js, fjs = d.getElementsByTagName(s)[0];
		if (d.getElementById(id))
			return;
		js = d.createElement(s);
		js.id = id;
		js.src = "//connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.6&appId=381324158709242";
		fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));
</script>

</head>
<body>

	<%
		CategoryDAO categoryDAO = new CategoryDAO();
		User users = null;
		if (session.getAttribute("user") != null) {
			users = (User) session.getAttribute("user");
		}
		Cart cart = (Cart) session.getAttribute("cart");
		if (cart == null) {
			cart = new Cart();
			session.setAttribute("cart", cart);
		}
	%>
	

	<!--header-->
	<div class="header">
		<div class="header-top">
			<div class="container">
				<div class="header-top-in">
					<div class="logo">
						<a href="${pageContext.request.contextPath}/"><img
							src="${root}/resources/images/logo.png" alt=" "></a>
					</div>
					<div class="header-in">
						<ul class="icon1 sub-icon1">
							<%
								if (users != null) {
							%>
							<li><a href="#"><%=users.getUser_email()%></a></li>
							<li><a href="${pageContext.request.contextPath}/logout">Log Out</a></li>
							<%
								}
							%>

							<!--   <li><a href="wishlist.jsp">WISH LIST (0)</a> </li>
                            <li><a href="account.jsp">  MY ACCOUNT</a></li>
                            <li><a href="#"> SHOPPING CART</a></li> -->



							<li><a href="checkout">CHECKOUT</a></li>
							<li><div class="cart">
									<a href="#" class="cart-in"> </a> <span> <%=cart.countItem()%></span>
								</div>
								<ul class="sub-icon1 list">
									<h3>Recently added items</h3>
									<div class="shopping_cart">

										<%
											for (Map.Entry<Long, Item> list : cart.getCartItems().entrySet()) {
										%>
										<div class="cart_box">
											<div class="message">
												<div class="alert-close">
													
													<a href="${pageContext.request.contextPath}/CartServlet/remove/<%=list.getValue().getProduct().getProduct_id()%>"></a>
												</div>
												<div class="list_img">
													<img
														src="${root}/resources/<%=list.getValue().getProduct().getProduct_image()%>"
														class="img-responsive" alt="">
												</div>
												<div class="list_desc">
													<h4>
														<%=list.getValue().getProduct().getProduct_name()%>
													</h4>
													<%=list.getValue().getQuantity()%>
													x<span class="actual"> $<%=list.getValue().getProduct().getProduct_price()%></span>
												</div>
												<div class="clearfix"></div>
											</div>
										</div>
										<%
											}
										%>


									</div>
									<div class="total">
										<div class="total_left">Cart Subtotal:</div>
										<div class="total_right">
											$<%=cart.totalCart()%></div>
										<div class="clearfix"></div>
									</div>
									<div class="login_buttons">
										<div class="check_button">
											<a href="checkout">Check out</a>
										</div>
										<div class="clearfix"></div>
									</div>
									<div class="clearfix"></div>
								</ul></li>
						</ul>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>

		<div class="header-bottom">
			<div class="container">
				<div class="h_menu4">
					<!-- <a class="toggleMenu" href="#">Menu</a> -->
					<ul class="nav">
						<li class="active"><a
							href="${pageContext.request.contextPath}/"><i> </i>Home</a></li>
						<li>Danh mục
							<ul class="drop">
								<c:forEach var="category" items="${categorys}">
								<li>
									<a
										href="${pageContext.request.contextPath}/product/${category.category_id}">${category.category_name}
									</a>
									
								</li>
							</c:forEach>
							</ul></li>

						<li><a href="contact">Contact </a></li>

					</ul>
					<script type="text/javascript" src="resources/js/nav.js"></script>
				</div>
			</div>
		</div>
		<div class="header-bottom-in">
			<div class="container">
				<div class="header-bottom-on">
					<p class="wel">
						Welcome visitor you can<a href="${pageContext.request.contextPath}/login"> login</a> or <a
							href="${pageContext.request.contextPath}/register"> create an account.</a>
					</p>
					<div class="header-can">

						<!--     <ul class="social-in">
                            <li><a href="#"><i> </i></a></li>
                            <li><a href="#"><i class="facebook"> </i></a></li>
                            <li><a href="#"><i class="twitter"> </i></a></li>					
                            <li><a href="#"><i class="skype"> </i></a></li>
                        </ul>	
                        
                        -->
						<div class="down-top">
							<form action="side" method="GET">
								<select class="in-drop" id="Price" name="Price" onchange="this.form.submit()">
									<option class="in-of">--Select Price--</option>
									<option value="Under 100.000" class="in-of">Under 100.000</option>
									<option value="100.000-300.000" class="in-of">100.000-300.000</option>
									<option value="300.000-500.000" class="in-of">300.000-500.000</option>
									<option value="More 500.000" class="in-of">More 500.000</option>
								</select>
							</form>
						</div>
						<div class="search">
							<form action="side" method="POST">
								<input type="text" name="Search"
									placeholder="Enter search name..."> <input
									type="submit" value="">
							</form>
						</div>

						<div class="clearfix"></div>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
	</div>

</body>
</html>
