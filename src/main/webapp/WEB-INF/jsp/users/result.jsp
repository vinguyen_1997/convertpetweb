<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="com.pnv.dao.SearchDao"%>
<%@page import="com.pnv.models.Product"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Searh</title>
<link href="resources/css/bootstrap.css" rel="stylesheet" type="text/css"
	media="all" />
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="resources/js/jquery.min.js"></script>
<!-- Custom Theme files -->
<!--theme-style-->
<link href="resources/css/style.css" rel="stylesheet" type="text/css" media="all" />
<!--//theme-style-->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords"
	content="Bonfire Responsive web template, Bootstrap Web Templates, Flat Web Templates, Andriod Compatible web template, 
              Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyErricsson, Motorola web design" />
</head>
<body>
	<%
		ArrayList<Product> listProduct = (ArrayList<Product>) request.getAttribute("products");
	%>
	<jsp:include page="header.jsp"></jsp:include>

	<br />
	<br />
	<!---->
	<div class="container">
		<div class="content">
			<div class="content-top">
				<h3 class="future">FEATURED</h3>
				<div class="content-top-in">
					<c:forEach var="product" items="${products }">
						<div class="col-md-3 md-col">
							<div class="col-md">
								<a
									href="${pageContext.request.contextPath}/single/${product.product_id}">
									<img
									src="<c:url value="/resources/${product.product_image}" />"
									alt="${product.product_name}" width="239px" height="207px" />
								</a>
								<div class="top-content">
									<h5>
										<a
											href="${pageContext.request.contextPath}/single/${product.product_id}">${product.product_name}</a>
									</h5>
									<div class="white">
										<a
											href="${pageContext.request.contextPath}/CartServlet/plus/${product.product_id}"
											class="hvr-shutter-in-vertical hvr-shutter-in-vertical2 ">ADD
											TO CART</a>
										<p class="dollar">
											<span class="in-dollar">$</span><span>${product.product_price}</span>
										</p>
										<div class="clearfix"></div>
									</div>

								</div>
							</div>
						</div>
					</c:forEach>
					<div class="clearfix"></div>
				</div>
			</div>

		</div>

	</div>

	<!---->


	<%-- <ul class="start">
				<li><a href="#"><i></i></a></li>
				<%
					for (int i = 1; i <= (total / 8) + 1; i++) {
				%>
				<li class="arrow"><a
					href="product?categoryID=<%=categoryID%>&pages=<%=i%>"><%=i%></a></li>
				<%
					}
				%>
				<li><a href="#"><i class="next"> </i></a></li>
			</ul> --%>


	<jsp:include page="footer.jsp"></jsp:include>

</body>
