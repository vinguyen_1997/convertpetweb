<%@page import="java.util.ArrayList"%>
<%@page import="com.pnv.models.Cart"%>
<%@page import="com.pnv.models.Product"%>
<%@page import="com.pnv.dao.ProductDao"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>product</title>
<link href="../resources/css/bootstrap.css" rel="stylesheet"
	type="text/css" media="all" />
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="../resources/js/jquery.min.js"></script>
<!-- Custom Theme files -->
<!--theme-style-->
<link href="../resources/css/style.css" rel="stylesheet" type="text/css"
	media="all" />
<!--//theme-style-->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords"
	content="Bonfire Responsive web template, Bootstrap Web Templates, Flat Web Templates, Andriod Compatible web template, 
              Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyErricsson, Motorola web design" />
<script type="application/x-javascript">
	
	
	
	 addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } 



</script>
<!--fonts-->
<link
	href='http://fonts.googleapis.com/css?family=Exo:100,200,300,400,500,600,700,800,900'
	rel='stylesheet' type='text/css'>
<!--//fonts-->
<script type="text/javascript" src="../resources/js/move-top.js"></script>
<script type="text/javascript" src="../resources/js/easing.js"></script>
<script type="text/javascript">
	jQuery(document).ready(function($) {
		$(".scroll").click(function(event) {
			event.preventDefault();
			$('html,body').animate({
				scrollTop : $(this.hash).offset().top
			}, 1000);
		});
	});
</script>
<!--slider-script-->
<script src="../resources/js/responsiveslides.min.js"></script>
<script>
	$(function() {
		$("#slider1").responsiveSlides({
			auto : true,
			speed : 500,
			namespace : "callbacks",
			pager : true,
		});
	});
</script>
<!--//slider-script-->
<script>
	$(document).ready(function(c) {
		$('.alert-close').on('click', function(c) {
			$('.message').fadeOut('slow', function(c) {
				$('.message').remove();
			});
		});
	});
</script>
<script>
	$(document).ready(function(c) {
		$('.alert-close1').on('click', function(c) {
			$('.message1').fadeOut('slow', function(c) {
				$('.message1').remove();
			});
		});
	});
</script>
</head>
<body>


	<jsp:include page="header.jsp"></jsp:include>

	<br />
	<br />
	<!---->
	<div class="container">
		<div class="content">
			<div class="content-top">
				<h3 class="future">FEATURED</h3>
				<div class="content-top-in">
					<c:forEach var="product" items="${products }">
						<div class="col-md-3 md-col">
							<div class="col-md">
								<a
									href="${pageContext.request.contextPath}/single/${product.product_id}"><img
									src="../resources/${product.product_image}"
									alt="${product.product_id}" /></a>
								<div class="top-content">
									<h5>
										<a
											href="${pageContext.request.contextPath}/single/${product.product_id}">${product.product_name}</a>
									</h5>
									<div class="white">
										<a
											href="${pageContext.request.contextPath}/CartServlet/plus/${product.product_id}"
											class="hvr-shutter-in-vertical hvr-shutter-in-vertical2 ">ADD
											TO CART</a>
										<p class="dollar">
											<span class="in-dollar">$</span><span>${product.product_price}</span>
										</p>
										<div class="clearfix"></div>
									</div>

								</div>
							</div>
						</div>

					</c:forEach>


					<div class="clearfix"></div>
				</div>
			</div>
			<!---->
			<%-- <ul class="start">
				<c:if test="${currentPage != 1}">
					<li><a href="${pageContext.request.contextPath}/product?categoryID=<%=categoryID%>&pages=${currentPage-1}"><i></i></a></li>
				</c:if>
				<%
					for (int i = 1; i <= maxPage; i++) {
				%>
				<li class="arrow"><a
					href="${pageContext.request.contextPath}/product?categoryID=<%=categoryID%>&pages=<%=i%>"><%=i%></a></li>
				<%
					}
				%>
				<c:if test="${currentPage lt maxPage }">
				<li><a href="${pageContext.request.contextPath}/product?categoryID=<%=categoryID%>&pages=${currentPage+1}"><i class="next"> </i></a></li>
				</c:if>
			</ul> --%>
		</div>
		<ul class="start">
		<li class="arrow"><a href=/petOnline/list/1>1</a> </li>
		<li class="arrow"><a href=/petOnline/list/2>2</a> </li>
		<li class="arrow"><a href=/petOnline/list/3>3</a> </li>
		<li class="arrow"><a href="#">></a>
	</ul>
	</div>

	</li>
	</li>
	<jsp:include page="footer.jsp"></jsp:include>

</body>
</html>
