<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>login</title>
<link href="resources/css/bootstrap.css" rel="stylesheet" type="text/css"
	media="all" />
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="resources/js/jquery.min.js"></script>
<!-- Custom Theme files -->
<!--theme-style-->
<link href="resources/css/style.css" rel="stylesheet" type="text/css" media="all" />
<!--//theme-style-->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords"
	content="Bonfire Responsive web template, Bootstrap Web Templates, Flat Web Templates, Andriod Compatible web template, 
              Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyErricsson, Motorola web design" />
</head>
<body>

	<jsp:include page="header.jsp"></jsp:include>

	<div class="container">
		<div class="account">
			<h2 class="account-in">login</h2>
			<form action="login" method="POST">
				<div>
					<p style="color: red"><%= session.getAttribute("error") %></p>
				</div>
				<div>
					<label for="email">User Name<span class="required">*</span></label><input type="email" name="email" pattern="[a-zA-Z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*" required placeholder="Enter you Email (xxx@xxx.xxx)">
				</div>
				<div>
					<label for="pass">Password <span class="required">*</span></label> <input type="password"
						name="pass" pattern=".{3,8}" required placeholder="Enter you Password (3-8 kí tự)">
				</div>
				<p class="requiredd">* Yêu cầu bắt buộc</p>
				<input type="hidden" value="login" name="command"> <input
					type="submit" value="Login">
			</form>
		</div>
	</div>

	<jsp:include page="footer.jsp"></jsp:include>


</body>
</html>