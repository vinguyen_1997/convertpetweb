<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>content</title>
</head>
<body>

	<!---->
	<div class="container">
		<div class="content">
			<div class="content-top">
				<h3 class="future">FEATURED</h3>
				<div class="content-top-in">
		<c:forEach var="product" items="${products }">
					<div class="col-md-3 md-col">
						<div class="col-md">
							<a href="${pageContext.request.contextPath}/single/${product.product_id}"> <img
								src="<c:url value="/resources/${product.product_image}" />" alt="${product.product_name}"
								width="239px" height="207px" /></a>
							<div class="top-content">
								<h5>
									<a href="${pageContext.request.contextPath}/single/${product.product_id}">${product.product_name}</a>
								</h5>
								<div class="white">
									<a
										href="${pageContext.request.contextPath}/CartServlet/plus/${product.product_id}"
										class="hvr-shutter-in-vertical hvr-shutter-in-vertical2 ">ADD
										TO CART</a>
									<p class="dollar">
										<span class="in-dollar">$</span><span>${product.product_price}</span>
									</p>
									<div class="clearfix"></div>
								</div>

							</div>
						</div>
					</div>
	</c:forEach>
	<div class="clearfix"></div>
	</div>
	
	<ul class="start">
		<li class="arrow"><a href=/petOnline/list/1>1</a> </li>
		<li class="arrow"><a href=/petOnline/list/2>2</a> </li>
		<li class="arrow"><a href=/petOnline/list/3>3</a> </li>
		<li class="arrow"><a href="#">></a>
	</ul>
	
					<div class="clearfix"></div>
				
			</div>

		</div>
	</div>

</body>
</html>
