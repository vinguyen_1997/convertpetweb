<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>contact</title>
<link href="resources/css/bootstrap.css" rel="stylesheet" type="text/css"
	media="all" />
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="resources/js/jquery.min.js"></script>
<!-- Custom Theme files -->
<!--theme-style-->
<link href="resources/css/style.css" rel="stylesheet" type="text/css" media="all" />
<!--//theme-style-->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

</head>
<body>

<jsp:include page="header.jsp"></jsp:include>
	<div class="container">
			<div class="contact">
			<h2 class=" contact-in">CONTACT</h2>
				
				<div class="col-md-6 contact-top">
					<h3>Info</h3>
					<div class="map">
						<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3833.890982889846!2d108.21524931441809!3d16.071145988879746!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x314218368c16c0d3%3A0xb16d0ccefe9f4300!2zODAgTMOqIER14bqpbiwgVGjhuqFjaCBUaGFuZywgUS4gSOG6o2kgQ2jDonUsIMSQw6AgTuG6tW5nLCBWaeG7h3QgTmFt!5e0!3m2!1svi!2s!4v1481282519994" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
					</div>			
					<p>Pet shop</p>
					<p>Kites Team</p>				
					<ul class="social ">
						<li><span><i > </i>80B Lê Duẩn, Hải Châu, Đà Nẵng </span></li>
						<li><span><i class="down"> </i>0964 259 207</span></li>
						<li><a href="mailto:bichvienle@gmail.com"><i class="mes"> </i>bichvienle@gmail.com</a></li>
					</ul>
				</div>
				<div class="col-md-6 contact-top">
					<h3>Want to work with me?</h3>
						<div>
							<span>Your Name </span>		
							<input type="text" value="" >						
						</div>
						<div>
							<span>Your Email </span>		
							<input type="text" value="" >						
						</div>
						<div>
							<span>Subject</span>		
							<input type="text" value="" >	
						</div>
						<div>
							<span>Your Message</span>		
							<textarea> </textarea>	
						</div>
						<input type="submit" value="SEND" >	
				</div>
			<div class="clearfix"> </div>
		</div>
	</div>

<jsp:include page="footer.jsp"></jsp:include>

</body>
</html>