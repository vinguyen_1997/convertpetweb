<%@page import="com.pnv.models.User"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>header</title>
    </head>
    <body>
	<%

		User users = new User();
		if (session.getAttribute("user") != null) {
			users = (User) session.getAttribute("user");
		}
	%>
        <div id="header">
            <div class="inHeader">
                <div class="mosAdmin">
                    Hallo, <%=users.getUser_email()%> Administrator<br>
                    <a href="#">Lihat website</a> | <a href="#">Help</a> | <a href="">Keluar</a>
                </div>
                <div class="clear"></div>
            </div>
        </div> 

    </body>
</html>
